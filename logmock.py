import getopt
import sys
from time import sleep
import os

def main(argv):
    file = ""
    period = 0
    rounds = 0
    address = ""
    port = ""

    try:
        opts, args = getopt.getopt(argv,"hf:p:n:i:P:")
    except getopt.GetoptError:
        print('slice.py -i <input_file> -n <seconds_by_slice>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('python oltmock.py -f <file> -p <period> -n <rounds> -i <ip> -P <port>')
            sys.exit()
        elif opt in ("-f"):
            file = arg
        elif opt in ("-p"):
            period = int(arg)
        elif opt in ("-n"):
            rounds = int(arg)
        elif opt in ("-i"):
            address = arg
        elif opt in ("-P"):
            port = arg

    #if case == 1:
    #    filepath = "gx_c1.log"
    #elif case == 2:
    #    filepath = "oltmock/gx_c2.log"
    #elif case == 5:
    #    filepath = "oltmock/gx_c5.log"

    for i in range(rounds):
        with open(file) as fp:
            line = fp.readline()
            while line:
		print("Enviando os logs...")
                os.system("logger -n " + address + " -P " + port + " " + "--rfc3164" + " \"" + line.strip() + "\"")
                sleep(period/1000)
                line = fp.readline()

if __name__ == "__main__":
    main(sys.argv[1:])
